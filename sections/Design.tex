% ----------------------------------------------------------------------------
% Chapter 1 - Introduction
% ----------------------------------------------------------------------------
\documentclass[thesis.tex]{subfiles}
\begin{document}

\chapter*{Design}\stepcounter{chapter}\addcontentsline{toc}{chapter}{Design}
Following the literature review, the OOK modulation method was chosen for this design. Given that OOK Is to be used, the following research question was raised:
How would one go about using the OOK method practically in a circuitry design, such that the signal is accurate enough to be picked up every time by the receiver?
A number of additional questions are to be considered as well:
\begin{itemize}
\item In what way can the fence noise be minimised?
\item Can we minimise the number of components as much as possible for the need of saving costs \& manufacturing changes?
\item How can we protect the circuit from High Voltage?
\item How do we design the modulating \& demodulating circuit?
\end{itemize}

The rest of this section outlines the design process.  Section 3.1 discusses modelling of the fence as a transmission line, \& frequency selection given loss \& frequency allocation constraints.  Section 3.2 then goes on to outline the design of the four parts of the circuit: modulator, demodulation, High Voltage Protection circuitry \& carrier wave generation.  Finally, simulation of the modulator \& demodulation circuits is discussed in Section 3.3.

\section{Simulation \& Frequency Selection}
With the proposed design the electrified fence will act as a transmission line, which can be modelled on a simulated program using a Distributed Element Model System \cite{ref12}.
 
 
\begin{figure}
	\centering
   \input{images/DistributedElementModel.tikz}
   \caption{Distributed Element Model for a 1 km Fence}\label{fig:DEMFence}
\end{figure} 
 
Referring to \ref{fig:DEMFence}, R1 represents the real resistance of a wire \& for agricultural \& security fencing the common material used is Steel. R2 is represented by insulation quality \& other loss factors such as grass touching the wire. C1 \& L1 can be found by measuring over a length of the steel conductor. Paul Thompson of Pakton Technologies has conducted tests \& has provided results from a 2.5cm diameter steel conductor spanning over 1km. These components are represented in our simulation program with the following values: R1= 50 $\Omega$; R2= 100 k$\Omega$; C1= 10 $\eta$F; L1= 1 $\mu$H. The Distributed Element Model System can be added 10 times such that a 10 km fence can be simulated.
 
The simulation program Lt-spice was used to simulate this system, the first step was to represent a bode plot of our systems output to determine the frequency range we can use, without having a large dB loss. \ref{fig:frequencySweep} represents the magnitude \& phase across all frequencies ranging from 10 kHz to 1 MHz. 
The first line is the 1 km test point \& it then increments by 1 km up until the 10 km mark has been simulated. The simulation calculates that to send a clear signal through a 10 km fence line the frequency range needs to not exceed 100 kHz.
\begin{figure}
	\centering
    \includegraphics[width=\textwidth]{images/frequencysweep.png}
   \caption{Phase vs Frequency \& dB vs Frequency of the circuit in Figure 2. 
Legend: Red = 1 km; Blue = 2 km; Aqua = 3 km; Dark Red = 4 km; Grey = 5 km; Dark Green = 6 km; Purple = 7 km; Gold = 8 km; Light Green = 9 km; Pink = 10 km,}\label{fig:frequencySweep}
\end{figure} 

\subsection{Frequency Chart Selection}
Frequency allocation is an important consideration when designing our signals, as we cannot have the receiver responding to signals from other sources in the proximity such as the radio or other Energisers. This is an important safety consideration, since if the Energiser were to turn on when a person is repairing the fence, serious injuries could result. As an international law, products that use communication techniques need to apply to certain radio-frequency spectrum allocations, this stops interconnecting signals between completely different devices. 

In Australia the standards for this are set by the ACMA \cite{ref17} whom specify the certain frequency spectra allowed in Australia. To bypass this problem a frequency chosen on the chart in Appendix 3  will need to be either on a free bandwidth or on a bandwidth with an object that does not affect the proposed design. Observing the chart, a frequency range between 90 kHz \& 100 kHz would be suitable, even though it is occupied. For the purposes of this design +95 kHz will be used as a starting point \& modifications will be made if it needs to be tweaked for future improvements.
  
\section{Circuit Design Concept}
The design of this circuit is split into 4 parts: Modulation, Demodulation, High Voltage Protection \& Carrier wave generation. The transmitter device will contain the Modulation, High Voltage Protection \& Carrier wave circuits whilst the receiver will contain just the Demodulation \& High Voltage Protection circuits. This design will then be finalised onto the Power Probe after the external devices pass the testing phase. The different designs will be discussed in the sub sections of this heading including the Component selections \& testing methods. Simulation is done on both the Modulation \& Demodulation circuits but physical testing will need to be done on the Carrier wave \& High Voltage protection circuit, which will be provided in future reports. 

\subsection{Component Selection}
In this design the following Integrated circuits were chosen:
\begin{itemize}
\item CD4066B – CMOS Quad Bilateral Switch
\item LMH6622 – Dual Wideband, Low Noise, 160 MHz, Operational Amplifier
\item LT1493CS – Operation Amplifier
\end{itemize}
The CD4066B is used in the modulating circuit; the LMH6622 was used as a means of generating a 95 kHz frequency wave \& the LT1493CS was used in the Demodulating circuit. More information on the circuits are specified in the following sections. The descriptions of the integrated circuits on the data sheet can also be viewed in Appendix 4, 5, 6 \& 7.

\subsection{OOK Modulating Circuit}
The design of the modulation circuit contains just the CD4066B component. CD4066B is a simple switch that will allow the frequency through when the enable pin is pulled high \& vice versa when pulled low. In theory this should create a perfect OOK signal which is proven in the simulation to testing stages. Refer to \ref{fig:CD4066BB} for the schematic of the modulating circuit.
\begin{figure}
	\centering
    \input{images/CD4066BCircuit.tikz}
    \caption{CD4066B} \label{fig:CD4066BB}
\end{figure}

\subsection{OOK De-Modulating Circuit}
A simple way for the demodulation of an ASK is to use an envelope detector which can be seen in \ref{fig:EnevelopeCircuit}. By referring to \cite{ref13} one can theoretically calculate what the voltage output \& peak of their system is. Observing equations \ref{eq:1}, \ref{eq:2} \& \ref{eq:3} the output waveform can be properly adjusted to make it as smooth as possible, where T represents $\frac{1}{CarrierFrequency (f_c)}$ \& $V_{peak}$ is the signals amplitude. A threshold Amplifier was used on the output of this envelope detector so that we could re-create a perfect DC signal. The threshold Amplifier circuit can be viewed in Figure \ref{fig:ThresholdCircuit}. Another addition to this circuit was a gaining amplifier on the input side of the circuit. This was used as the fence is assumed to apply a dB loss to the signal being sent. The gain amplifier can be seen on Figure \ref{fig:GainCircuit}.
\begin{equation}\label{eq:1}
\tau = R*C
\end{equation}
\begin{equation}\label{eq:2}
V_{1_{peak}}  \approx V_{peak} * \bigg[1 - \frac{T}{\tau}\bigg]	
\end{equation}
\begin{equation}\label{eq:3}
\Delta V_{ripple} =\frac{V_{1_{peak}} * T}{\tau}
\end{equation}
\begin{equation}\label{eq:4}
V_{threshold} = \frac{V_{cc} * R_2}{R_2 + R_1 }
\end{equation}
\begin{equation}\label{eq:5}
Voltage Gain  = \frac{R_2+R_1}{R_1}	
\end{equation}

 \begin{figure}
\begin{subfigure}{0.5\linewidth}
	\centering
    \resizebox{0.9\linewidth}{!}{\input{images/GainCircuit.tikz}}
    \caption{Gain Amplifier Circuit} \label{fig:GainCircuit}
  \end{subfigure}
  \begin{subfigure}{.45\textwidth}
	\centering
    \resizebox{\linewidth}{!}{\input{images/ThresholdCircuit.tikz}}
    \caption{Voltage Threshold Amplifier Circuit} \label{fig:ThresholdCircuit}
  \end{subfigure}
  \begin{subfigure}{\linewidth}
	\centering
  	\resizebox{0.45\linewidth}{!}{\input{images/EnvelopeDetector.tikz}}
    \caption{Peak/Envelope Detector Circuit} \label{fig:EnevelopeCircuit}
  \end{subfigure}
 \caption{De-Modulation Circuit} \label{fig:Demodulation}
\end{figure}

\subsection{High Voltage Protection Circuit}
Since we are dealing with high voltage Energisers, we need to protect the transmitter \& receiver circuit from high voltage. To do this high power rating resistors will need to be used in conjunction with a MOV. The MOV used is rated at 25Vac and will clamp the voltage at 31V, meaning this is the maximum voltage that is allowed to pass. MOV act like a zener diode however instead of allowing a maximum current through it allows voltage \cite{ref14}. Two 4700$\Omega$ resistors are placed in series and then connected to a parallel MOV connecting to ground. To protect the circuitry the current will also need to be limited and to do so a clamping diode circuit was used. A 3300$\Omega$ resistor was used so that when the max 31V was passed through, the maximum current will be rated at 10mA. The diodes will then need to handle this rating. Since the maximum current produced will be 10mA, the circuit will not be destroyed. Figure \ref{fig:ProtectionCircuits} displays the schematic representation of this circuit.

\begin{figure}
	\centering
    \input{images/protectionReceiver.tikz}
    \caption{High Voltage (10kV) Protection Circuit} \label{fig:ProtectionCircuits}
\end{figure} 
 
\subsection{Frequency Generator Circuit}
Referring to \cite{ref15} it can be seen that a frequency can be created from two operation amplifiers with passive components. The LMH6622 can handle large frequency ranges \& should not fail around the chosen 95 kHz. Figure \ref{fig:FreqGenCircuit} is the circuit designed for this project \& to get the passive components, Walter Bacharowsk \cite{ref15} specifies certain formulas for each component. R1, R2 \& R3 are all the same at 1 k$\Omega$ \& this helps minimise errors in the created frequency. Refer to Figure \ref{fig:FreqGenCircuit} for the values chosen as well as equations \ref{eq:6}, \ref{eq:7} \& \ref{eq:8} for how they were calculated.
\begin{equation}\label{eq:6}
R4 = R6 = \frac{0.5}{0.693 * C1 * f}		
\end{equation} 
\begin{equation}\label{eq:7}
R5 = \frac{1}{8.8856 * C1 * f} 		
\end{equation} 
\begin{equation}\label{eq:8}
C2 = C1 * 2 		
\end{equation} 


\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/95khzFreqGen.png}
  \caption{95kHz Frequency Generator Circuit}\label{fig:FreqGenCircuit}
\end{figure} 
 
\section{Simulation Testing and Value Selection}
\subsection{Lossless Transmission Line Testing}
A CD4066B model was created in LTSpice to simulate the output signal from this device. Through this simulation it can be seen that it works perfectly and that the OOK is created exactly how one would like it leaving no further modifications to the modulating circuit. The working modulating circuit was then connected directly to the demodulation circuit. This models an ideal situation where the signal input to the demodulator is exactly as generated by the modulator – but unlikely to occur in practice Refer to Figure \ref{fig:SimulatedModOutput} for the output of modulating circuit.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/ModCircuitSimulation.png}
  \caption{Simulated Output Characteristic of the Modulation Circuit}\label{fig:SimulatedModOutput}
\end{figure} 

Assuming no losses in the transmission line the next thing to do was to choose values for the demodulation circuit. A typical value of 1$nF$ for the capacitor was chosen which left for the resistance to be chosen. The resistance had to be large enough such that $\tau> \frac{1}{f_c}$ was satisfied. A value of 100 $k\Omega$ was chosen, this gave the theoretical values: $V_{1_{peak}}  = 4.52 V$ ,$\Delta V_{ripple} = 0.5 V$. This was also simulated and gave the values $V_{1_{peak}}  = 4.32 V$ ,$\Delta V_{ripple} = 0.4 V$ where the ideal diode could be the error margin between the theoretical and simulation values. The values on the simulation can be observed on Figure \ref{fig:SimulatedEnvelopeOutput}.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/EnvelopeCircuitSimulation}
  \caption{ Output Signal from the Envelope Detector Circuit}\label{fig:SimulatedEnvelopeOutput}
\end{figure} 


Having the ripple as low as possible allows us to apply a threshold amplifier on to the end of this signal so that we can then recreate a digital signal output. A voltage divider connected to the amplifier sets a limit where if the signal is above this voltage it will then output it as a high (5 V) and if not it will be a low (0 V). To set a threshold of your choosing refer to equation \ref{eq:4}. The threshold chosen is 3.5 V which makes R1= 10 $k\Omega$ and R2= 24 $k\Omega$. Referring to Figure \ref{fig:SimulatedTransmissionOutput} it can be seen that the output is recreated with a smaller amplitude then the sent signal likely due to the amplifier component. The widths are also slightly lagging due to a slight transmission delay.
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/LosslessTransmissionLineINvsOUT}
  \caption{Input vs Output Signal for a Lossless Transmission Line}\label{fig:SimulatedTransmissionOutput}
\end{figure} 

\subsection{Fence Transmission Line Testing}
The fence line transmission from Figure \ref{fig:DEMFence} can now be added to the simulation process where it will be used to simulated a 10 km fence line. The modulated signal is attached to the 10 km fence line as shown in Figure \ref{fig:10kmFenceCircuit} and from here each node representing 1km can also be tested to find the losses. The 1 km, 5 km and 10 km signals are represented in Figure \ref{fig:10kmFenceSim}. It can be seen that there is about a 10 dB loss in the 10 km fence. This could prove a problem when outputting to the envelope detector, to combat this a gain amplification was implemented on the input of the envelope detector. Using the 10 dB loss in the 10 km fence and equation \ref{eq:5} for the gain amplifier, the passive components were chosen; R1 = 1 $k\Omega$, R2= 10 $k\Omega$. The output of the gain amplifier can be seen in Figure \ref{fig:GainSim}.

\begin{figure}
  \centering
  \resizebox{ \linewidth}{!}{\input{images/10kmFence.tikz}}
  \caption{ Circuit Representation of a 10 km fence line}\label{fig:10kmFenceCircuit}
\end{figure} 

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/FenceTransmissionLineINvsOut}
  \caption{Input Signal vs Signal Output at 1 km, 5 km and 10 km of a Fence Transmission Line}\label{fig:10kmFenceSim}
\end{figure} 

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/GainCircuitOutput}
  \caption{Output for a Gain Amplifier of 10 dB}\label{fig:GainSim}
\end{figure} 

Since a lossy transmission line is used, the envelope detector passive component values will need to be altered slightly from the original design. Experimentation with component values in the simulation was used to obtain the least amount of noise. The capacitor value was kept constant, while the resistance value was modified. After observing output simulations, it was found that a resistance value of 25 $k\Omega$ provided the best signal outcome where the output can be viewed from Figure \ref{fig:EnvelopCircuit10kmFence}.


In addition, the output of the envelope detector will need to be observed to adjust the threshold voltage amplifier accordingly. As shown in Figure \ref{fig:ThresholdVoltageCircuit}, it can be seen that a threshold of 2.5 V can be used, allowing for slight peak variation errors.
After all components have been adjusted, the overall output vs input signal for a fence transmission line of 10km can be viewed in Figure \ref{fig:10kmINvsOUT}. As can be seen in Figure \ref{fig:10kmINvsOUT}, the output is correctly detected. The overall design circuitry to get this output can be viewed in Appendix 8. 9 and 10.


\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/EnvelopCircuit10kmFence.png}
  \caption{Envelope Detector Output Signal for a 10 km fence}\label{fig:EnvelopCircuit10kmFence}
\end{figure} 


\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/ThresholdVoltageCircuit.png}
  \caption{Threshold Voltage Marked on the Envelope Detectors Signal}\label{fig:ThresholdVoltageCircuit}
\end{figure} 


\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{images/10kmINvsOUT.png}
  \caption{Input vs Output Signal for a 10 km Fence}\label{fig:10kmINvsOUT}
\end{figure} 

\end{document}
